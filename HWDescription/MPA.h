/*!

        \file                   MPA.h
        \brief                  MPA Description class, config of the MPAs
        \author                 Lorenzo BIDEGAIN
        \version                1.0
        \date                   25/06/14
        Support :               mail to : lorenzo.bidegain@gmail.com

 */

#ifndef MPA_h__
#define MPA_h__

#include "ChipRegItem.h"
#include "FrontEndDescription.h"
#include "ReadoutChip.h"
#include "Utils/Exception.h"
#include "Utils/Visitor.h"
#include "Utils/easylogging++.h"
#include <iostream>
#include <map>
#include <set>
#include <stdint.h>
#include <string>
#include <utility>

/*!
 * \namespace Ph2_HwDescription
 * \brief Namespace regrouping all the hardware description
 */
namespace Ph2_HwDescription
{
using MPARegPair = std::pair<std::string, ChipRegItem>;
using CommentMap = std::map<int, std::string>;

class MPA : public ReadoutChip
{
  public:
    static constexpr size_t nRows = NSSACHANNELS;
    static constexpr size_t nCols = NMPAROWS;

    MPA(uint8_t pBeBoardId, uint8_t pFMCId, uint8_t pOpticalGroupId, uint8_t pHybridId, uint8_t pChipId, uint8_t pPartnerId, const std::string& filename);
    // C'tors with object FE Description
    MPA(const FrontEndDescription& pFeDesc, uint8_t pChipId, uint8_t pPartnerId, const std::string& filename);

    MPA(const MPA&) = delete;

    using MPARegPair = std::pair<std::string, ChipRegItem>;
    uint8_t fPartnerId;
    uint8_t getPartid() { return fPartnerId; }
    void    loadfRegMap(const std::string& filename) override;

    std::stringstream getRegMapStream() override;

    bool isDACLocal(const std::string& dacName) override
    {
        if((dacName.find("TrimDAC", 0, 9) != std::string::npos) or (dacName.find("ThresholdTrim") != std::string::npos))
            return true;
        else
            return false;
    }
    uint8_t getNumberOfBits(const std::string& dacName) override
    {
        if((dacName.find("TrimDAC_C", 0, 9) != std::string::npos) or (dacName.find("ThresholdTrim") != std::string::npos))
            return 5;
        else
            return 8;
    }

    // row, col starts at index 0, global pix number starts at number 1

    std::pair<uint32_t, uint32_t> PNlocal(const uint32_t PN) { return std::pair<uint32_t, uint32_t>((PN + 1) / 120 + 1, ((PN - 1) % 120) + 1); }

    uint32_t getNumberOfChannels() const override { return NMPAROWS * NSSACHANNELS; }

    uint32_t PNglobal(std::pair<uint32_t, uint32_t> PC) { return (PC.first - 1) * 120 + (PC.second - 1) + 1; }
};

struct MPARegItemComparer
{
    bool operator()(const MPARegPair& pRegItem1, const MPARegPair& pRegItem2) const;
};
} // namespace Ph2_HwDescription

#endif
